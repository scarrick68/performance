// Getting Infinity as a result for small sizes like 1000.
// Need to figure out how to handle big numbers in JS.

function recursiveFactorial(n){
	if(n === 1){
		return 1;
	}
	return n * recursiveFactorial(n - 1);
}

function iterativeFactorial(n){
	var total = 1
	for(var i = 1; i <= n; i++){
		total = total * i;
		console.log(i, ':', total)
	}
	return total;
}

var temp1 = recursiveFactorial(100);
console.log(temp1);
var temp2 = iterativeFactorial(100);
console.log(temp2)