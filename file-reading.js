// Solve multi word anagrams
var fs = require('fs')
var readline = require('readline')

// Use a stream to read data into the program
var startline = new Date().getTime()
var lineReader = readline.createInterface({
	input: fs.createReadStream('/Users/scarrick/Downloads/wordlist')
})

lineReader.on('line', function(line){
	//console.log(line)
})

lineReader.on('close', function(){
	var endline = new Date().getTime()
	console.log('line reader', endline - startline)
})

// Read the entire file into memory
var start = new Date().getTime()
fs.readFile('/Users/scarrick/Downloads/wordlist', function(err, data){
	var end = new Date().getTime()
	console.log('full file', end - start)
})

// Use readable stream
var startStream = new Date().getTime()
var readStream = fs.createReadStream('/Users/scarrick/Downloads/wordlist')
readStream.on('data', (chunk)=>{})
readStream.on('end', ()=>{
	var endStream = new Date().getTime()
	console.log('readable stream', endStream - startStream)
})

var timer4 = new Date().getTime();
var startEndStream = fs.createReadStream('/Users/scarrick/Downloads/wordlist')
startEndStream.on('readable', ()=> {
	while (null !== (chunk = startEndStream.read(900000))) {
    	console.log('got %d bytes of data', chunk.length);
  	}
})

startEndStream.on('end', ()=>{
	var timer5 = new Date().getTime();
	console.log('variable buffer size in pause mode:', timer5-timer4)
})

var timer4 = new Date().getTime();
var startEndtream = fs.createReadStream('/Users/scarrick/Downloads/wordlist')
startEndStream.on('readable', ()=> {
	while (null !== (chunk = startEndStream.read(900000))) {
    	console.log('got %d bytes of data', chunk.length);
  	}
})

startEndStream.on('end', ()=>{
	var timer5 = new Date().getTime();
	console.log('variable buffer size in pause mode:', timer5-timer4)
})
// Reading the full file into memory is ~4x faster than streaming it in
// line by line. Tested on file ~1 MB. Read Stream and line reader have 
// roughly same performance. readable stream faster by ~3-4 ms in this test.